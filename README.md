# Power Up - Java x AWS

## First week task: User register (Driver and passenger)

The objective of the bootcamp first week is to develop a REST API
that allows to create a user and query it from a relational database.

## Acceptance criteria
- When registering a user, you should only be able to register a user if you have all the following fields:
  NAME, SURNAME, PHONE NUMBER, ADDRESS, EMAIL AND PASSWORD.


- The characteristics of the password must be: a minimum length of 8 characters and a maximum of 15,
  it must contain uppercase lowercase letters, numbers and a character such as “*_-”.


- Validate that it has an adequate mail structure.


- Each service must have its own unit tests.


- The use of good practices in implementations will be taken into account.


- An H2 database is required to be used for this first deliverable.