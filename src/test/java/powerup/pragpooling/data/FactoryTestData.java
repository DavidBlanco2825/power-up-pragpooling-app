package powerup.pragpooling.data;

import powerup.pragpooling.bussiness.User;

import java.util.List;

public class FactoryTestData {
    public static User getUser() {
        User user = new User();
        user.setId(1L);
        user.setFirstName("John");
        user.setLastName("Doe");
        user.setPhoneNumber("5051930809");
        user.setAddress("Somewhere Nowhere");
        user.setEmailAddress("johndoe@nowhere.com");
        user.setPassword("12345678Aa_?");
        return user;
    }

    public static User getNullUser() {
        return null;
    }

    public static User getUserWithBlankName() {
        User user = new User();
        user.setFirstName("");
        user.setLastName("Doe");
        user.setPhoneNumber("5051930809");
        user.setAddress("Somewhere Nowhere");
        user.setEmailAddress("johndoe@nowhere.com");
        user.setPassword("12345678Aa_?");
        return user;
    }


}
