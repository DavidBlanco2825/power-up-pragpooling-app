package powerup.pragpooling.bussiness;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import powerup.pragpooling.data.FactoryTestData;
import powerup.pragpooling.persistence.UserRepository;


import java.util.Optional;

import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
class UserServiceTest {

    @InjectMocks
    UserService userService;

    @Mock
    UserRepository userRepository;

    @Test
    void shouldFindAUserById() {
        //Given
        User expectedUser = FactoryTestData.getUser();

        //When
        when(userRepository.findById(anyLong())).thenReturn(Optional.of(expectedUser));

        User user = userService.findUserById(expectedUser.getId());

        //Then
        Assertions.assertNotNull(user);
    }

    @Test
    void shouldReturnNullIfUserIsNotFound() {
        //Given
        User expectedUser = null;

        //When
        when(userRepository.findById(anyLong())).thenReturn(Optional.ofNullable(expectedUser));

        User user = userService.findUserById(anyLong());

        //Then
        Assertions.assertEquals(null, user);
    }

    @Test
    void shouldSaveAnUser() throws Exception {
        //Given
        User expectedUser = FactoryTestData.getUser();

        //When
        when(userRepository.save(any(User.class))).thenReturn(expectedUser);

        User user = userService.save(FactoryTestData.getUser());

        //Then
        verify(userRepository).save(any(User.class));
    }

    @Test
    void shouldSaveNonNullUser() throws Exception {
        //Given
        User expectedUser = FactoryTestData.getUser();

        //When
        when(userRepository.save(any(User.class))).thenReturn(expectedUser);

        //Then
        User user = userService.save(FactoryTestData.getUser());
        Assertions.assertNotNull(user);
    }

    @Test
    void savedUserIdNameShouldMatch() throws Exception {
        //Given
        User expectedUser = FactoryTestData.getUser();

        //When
        when(userRepository.save(any(User.class))).thenReturn(expectedUser);

        //Then
        User user = userService.save(FactoryTestData.getUser());
        Assertions.assertEquals(user.getId(), expectedUser.getId());
    }

    @Test
    void savedUserFirstNameShouldMatch() throws Exception {
        //Given
        User expectedUser = FactoryTestData.getUser();

        //When
        when(userRepository.save(any(User.class))).thenReturn(expectedUser);

        //Then
        User user = userService.save(FactoryTestData.getUser());
        Assertions.assertEquals(user.getFirstName(), expectedUser.getFirstName());
    }

    @Test
    void savedUserLastNameShouldMatch() throws Exception {
        //Given
        User expectedUser = FactoryTestData.getUser();

        //When
        when(userRepository.save(any(User.class))).thenReturn(expectedUser);

        //Then
        User user = userService.save(FactoryTestData.getUser());
        Assertions.assertEquals(user.getLastName(), expectedUser.getLastName());
    }

    @Test
    void savedUserPhoneNumberShouldMatch() throws Exception {
        //Given
        User expectedUser = FactoryTestData.getUser();

        //When
        when(userRepository.save(any(User.class))).thenReturn(expectedUser);

        //Then
        User user = userService.save(FactoryTestData.getUser());
        Assertions.assertEquals(user.getPhoneNumber(), expectedUser.getPhoneNumber());
    }

    @Test
    void savedUserAddressShouldMatch() throws Exception {
        //Given
        User expectedUser = FactoryTestData.getUser();

        //When
        when(userRepository.save(any(User.class))).thenReturn(expectedUser);

        //Then
        User user = userService.save(FactoryTestData.getUser());
        Assertions.assertEquals(user.getAddress(), expectedUser.getAddress());
    }

    @Test
    void savedUserEmailShouldMatch() throws Exception {
        //Given
        User expectedUser = FactoryTestData.getUser();

        //When
        when(userRepository.save(any(User.class))).thenReturn(expectedUser);

        //Then
        User user = userService.save(FactoryTestData.getUser());
        Assertions.assertEquals(user.getEmailAddress(), expectedUser.getEmailAddress());
    }

    @Test
    void savedUserPasswordShouldMatch() throws Exception {
        //Given
        User expectedUser = FactoryTestData.getUser();

        //When
        when(userRepository.save(any(User.class))).thenReturn(expectedUser);

        //Then
        User user = userService.save(FactoryTestData.getUser());
        Assertions.assertEquals(user.getPassword(), expectedUser.getPassword());
    }
}