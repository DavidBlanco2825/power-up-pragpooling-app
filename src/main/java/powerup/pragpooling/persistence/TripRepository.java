package powerup.pragpooling.persistence;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import powerup.pragpooling.bussiness.Trip;

@Repository
public interface TripRepository extends CrudRepository<Trip, Integer> {
}
