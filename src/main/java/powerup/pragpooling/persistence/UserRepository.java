package powerup.pragpooling.persistence;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import powerup.pragpooling.bussiness.User;

@Repository
public interface UserRepository extends CrudRepository<User, Long> {
}
