package powerup.pragpooling.bussiness;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data @NoArgsConstructor
public class Schedule {
    private Date date;
    private Integer hour;
}
