package powerup.pragpooling.bussiness;

import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import powerup.pragpooling.persistence.TripRepository;

@Service
@AllArgsConstructor
public class TripService {

    @Autowired
    private final TripRepository tripRepository;

    public Trip saveTrip(Trip trip) {
        Trip tripToSave = new Trip(
                trip.getItinerary(),
                trip.getSchedule(),
                trip.getAvailableSlots()
        );

        return TripRepository.;
    }
}
