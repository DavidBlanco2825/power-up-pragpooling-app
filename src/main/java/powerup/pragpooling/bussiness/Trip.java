package powerup.pragpooling.bussiness;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.validation.constraints.Size;
import java.util.List;


@Entity
@Data @NoArgsConstructor
@Table
public class Trip {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY) @Column
    private Integer tripId;

    @Size(min = 2)
    private List<Neighborhood> itinerary;

    @Size(min = 1)
    private Schedule schedule;

    @Size(min = 1, max = 4)
    private Integer availableSlots;

    public Trip(List<Neighborhood> itinerary, Schedule schedule, Integer availableSlots) {
        this.itinerary = itinerary;
        this.schedule = schedule;
        this.availableSlots = availableSlots;
    }
}
