package powerup.pragpooling.bussiness;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data @NoArgsConstructor
public class Neighborhood {
    private Integer id;
    private String name;
    private String description;

}
