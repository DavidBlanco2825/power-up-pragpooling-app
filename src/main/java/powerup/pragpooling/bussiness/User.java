package powerup.pragpooling.bussiness;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

@Entity
@Data @NoArgsConstructor
@Table(name = "userData")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY) @Column
    private Long id;

    @NotBlank @Column(name = "first_name")
    private String firstName;

    @NotBlank @Column(name = "last_name")
    private String lastName;

    @NotBlank @Column(name = "phone_number")
    private String phoneNumber;

    @NotBlank @Column
    private String address;

    @NotBlank @Column(name = "email_address")
    @Email
    private String emailAddress;

    @NotBlank @Column
    @Pattern(regexp = "^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[@$!%*?&])[A-Za-z\\d@$!%*?_\\-&]{8,15}$")
    private String password;

    public User(String firstName, String lastName, String phoneNumber, String address, String emailAddress, String password) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.phoneNumber = phoneNumber;
        this.address = address;
        this.emailAddress = emailAddress;
        this.password = password;
    }
}
